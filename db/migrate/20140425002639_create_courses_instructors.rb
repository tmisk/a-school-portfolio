class CreateCoursesInstructors < ActiveRecord::Migration
  def change
    create_table :courses_instructors do |t|
      t.references :course, index: true
      t.references :instructor, index: true

      t.timestamps
    end
  end
end
