class CreateProjectsStudents < ActiveRecord::Migration
  def change
    create_table :projects_students do |t|
      t.references :project, index: true
      t.references :student, index: true

      t.timestamps
    end
  end
end
