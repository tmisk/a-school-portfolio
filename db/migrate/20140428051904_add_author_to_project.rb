class AddAuthorToProject < ActiveRecord::Migration
  def change
    add_column :projects, :author, :string
    add_column :projects, :author_email, :string
  end
end
