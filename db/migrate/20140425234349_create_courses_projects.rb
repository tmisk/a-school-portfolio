class CreateCoursesProjects < ActiveRecord::Migration
  def change
    create_table :courses_projects do |t|
      t.references :course, index: true
      t.references :project, index: true

      t.timestamps
    end
  end
end
