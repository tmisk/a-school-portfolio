class ChangeEmailAdmin < ActiveRecord::Migration
  def change
  	rename_column :admins, :email, :contact_email
  end
end
