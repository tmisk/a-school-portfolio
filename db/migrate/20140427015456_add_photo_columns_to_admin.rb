class AddPhotoColumnsToAdmin < ActiveRecord::Migration
  def change
  	add_attachment :admins, :photo
  end
end
