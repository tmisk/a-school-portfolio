class CreateInstructorsProjects < ActiveRecord::Migration
  def change
    create_table :instructors_projects do |t|
      t.references :instructor, index: true
      t.references :project, index: true

      t.timestamps
    end
  end
end
