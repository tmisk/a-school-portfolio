class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :name
      t.string :surname
      t.string :student_number
      t.string :email
      t.string :website

      t.timestamps
    end
  end
end
