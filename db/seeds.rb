Course.destroy_all
Instructor.destroy_all
Student.destroy_all
Project.destroy_all

c1 = Course.create(name:'Projektowanie Graficzne', abbreviation: 'PGRAF', description: 'kurs o proektowaniu graficznym')
c2 = Course.create(name:'Malarstwo', abbreviation: 'MAL', description: 'kurs o malarstwie')
c3 = Course.create(name:'Fotografia', abbreviation:'FOT', description: 'kurs o fotografii')

i1 = Instructor.create(name:Faker::Name.first_name, surname:Faker::Name.last_name, contact_email:Faker::Internet.email, email:Faker::Internet.email, password: 'password', password_confirmation: 'password')
i2 = Instructor.create(name:Faker::Name.first_name, surname:Faker::Name.last_name, contact_email:Faker::Internet.email, email:Faker::Internet.email, password: 'password', password_confirmation: 'password')
i3 = Instructor.create(name:Faker::Name.first_name, surname:Faker::Name.last_name, contact_email:Faker::Internet.email, email:Faker::Internet.email, password: 'password', password_confirmation: 'password')
i4 = Instructor.create(name:Faker::Name.first_name, surname:Faker::Name.last_name, contact_email:Faker::Internet.email, email:Faker::Internet.email, password: 'password', password_confirmation: 'password')
i5 = Instructor.create(name:Faker::Name.first_name, surname:Faker::Name.last_name, contact_email:Faker::Internet.email, email:Faker::Internet.email, password: 'password', password_confirmation: 'password')
i6 = Instructor.create(name:Faker::Name.first_name, surname:Faker::Name.last_name, contact_email:Faker::Internet.email, email:Faker::Internet.email, password: 'password', password_confirmation: 'password')




s1 = Student.create(name:Faker::Name.first_name, surname:Faker::Name.last_name, email:Faker::Internet.email)
s2 = Student.create(name:Faker::Name.first_name, surname:Faker::Name.last_name, email:Faker::Internet.email)

c1.instructors << i1
c2.instructors << i2
c3.instructors << i1 << i2

4.times do
	p = Project.new(title:Faker::Lorem.word, description:Faker::Lorem.sentence)
	p.courses << c1
	p.instructors << i1
	p.students << s1
	p.save
end

4.times do
	p = Project.new(title:Faker::Lorem.word, description:Faker::Lorem.sentence)
	p.courses << c2
	p.instructors << i2
	p.students << s2
	p.save
end

4.times do
	p = Project.new(title:Faker::Lorem.word, description:Faker::Lorem.sentence)
	p.courses << c3
	p.instructors << i1 << i2 << i3
	p.students << s1 << s2
	p.save
end