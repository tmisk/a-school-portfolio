class AdminsController < ApplicationController
	before_action :set_admin, only: [:show, :update, :destroy]

	def index
    	@admins = Admin.all
  	end

	def show
	end

	def new
		redirect_to new_admin_registration_path
	end

	def edit
		redirect_to edit_admin_registration_path
	end

	def destroy
		@admin.destroy
		redirect_to admins_url
	end

	private
	def set_admin
      	@admin = Admin.find(params[:id])
	end
	
	def admin_params
	    params.require(:admin).permit(:name, :surname, :contact_email, :photo)
	end
end
