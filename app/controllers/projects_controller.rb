class ProjectsController < ApplicationController
	before_action :set_project, only: [:show, :destroy, :mercury_update, :update]


	def index
    	@projects = Project.all
  	end

	def show
		@instructors = @project.instructors.all
		@courses = @project.courses.all
	end

	def new
		@project = Project.new
	end

	def create 
		@project = Project.new(project_params)
  		@project.save
  		redirect_to @project
	end

	def update
		@project.image = project_params[:image]
		@project.save!

		redirect_to '/editor' + project_path
	end

	def mercury_update
		@project.title = params[:content][:project_title][:value]
		@project.description = params[:content][:project_description][:value]
		@project.author = params[:content][:project_author][:value]
		@project.author_email = params[:content][:project_author_email][:value]
	  	@project.save!
	 	render text: ''
	end

	def destroy
		@project.destroy
		redirect_to projects_url
	end

	private
	def set_project
      	@project = Project.find(params[:id])
	end
	
	def project_params
	    params.require(:project).permit(:title, :description, :image, :author, :author_email)
	end
end
