class InstructorsController < ApplicationController
	before_action :set_instructor, only: [:show]

	def index
		@instructors = Instructor.all
	end

	def show
		@courses = @instructor.courses.all
	end
	

	private
	def set_instructor
      	@instructor = Instructor.find(params[:id])
	end
	
end
