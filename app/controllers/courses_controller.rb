class CoursesController < ApplicationController
	before_action :set_course, only: [:show, :edit, :destroy, :mercury_update, :update]
	before_action :authenticate_admin!, only: [:new, :create, :edit, :mercury_update, :destroy, :update]

	def index
    	@courses = Course.all
  	end

	def show
	end

	def update
		@course.image = course_params[:image]
		@course.save!
		# @course.update(course)
		redirect_to '/editor' + course_path
	end

	def new
		@course = Course.new
	end

	def create 
		@course = Course.new(course_params)
  		@course.save
  		redirect_to @course
	end

	def edit
		redirect_to '/editor' + course_path
	end

	def mercury_update
		@course.name = params[:content][:course_name][:value]
		@course.abbreviation = params[:content][:course_abbreviation][:value]
		@course.description = params[:content][:course_description][:value]
	  	@course.save!
	 	render text: ''
	end

	def destroy
		@course.destroy
		redirect_to courses_url
	end

	private
	def set_course
      	@course = Course.find(params[:id])
	end
	
	def course_params
	    params.require(:course).permit(:name, :abbreviation, :description, :image)
	end
end
