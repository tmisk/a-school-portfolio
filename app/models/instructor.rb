class Instructor < Admin
	has_many :courses_instructors, dependent: :destroy
	has_many :courses, through: :courses_instructors

	has_many :instructors_projects, dependent: :destroy
	has_many :projects, through: :instructors_projects
end