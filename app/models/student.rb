class Student < ActiveRecord::Base
	has_many :projects_students, dependent: :destroy
	has_many :projects, through: :projects_students
end
