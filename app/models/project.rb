class Project < ActiveRecord::Base
	has_many :courses_projects, dependent: :destroy
	has_many :courses, through: :courses_projects

	has_many :projects_students, dependent: :destroy
	has_many :students, through: :projects_students

	has_many :instructors_projects, dependent: :destroy
	has_many :instructors, through: :instructors_projects

	has_attached_file :image, :styles => { :medium => "200x200>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  	validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
  	# validates_attachment_presence :image, false
	#TUTAJ!!!!!!!!!!


end

