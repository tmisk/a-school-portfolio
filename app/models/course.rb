class Course < ActiveRecord::Base
	has_many :courses_instructors, dependent: :destroy
	has_many :instructors, through: :courses_instructors

	has_many :courses_projects, dependent: :destroy
	has_many :projects, through: :courses_projects

	has_attached_file :image, :styles => { :medium => "200x200>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  	validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
end